package AerialVehicles;


import Entities.Coordinates;
import enums.FlightStatus;

public abstract class AerialVehicle {
    private int hoursSienceRepair;
    private int maxHoursToRepair;
    private Coordinates base;
    private FlightStatus status;

    public AerialVehicle(int hoursSienceRepair, int maxHoursToRepair, FlightStatus status, Coordinates base) {
        this.setHoursSienceRepair(hoursSienceRepair);
        this.setMaxHoursToRepair(maxHoursToRepair);
        this.setStatus(status);
        this.setBase(base);
    }

    private int getMaxHoursToRepair() {
        return maxHoursToRepair;
    }

    public void setMaxHoursToRepair(int maxHoursToRepair) {
        this.maxHoursToRepair = maxHoursToRepair;
    }

    public int getHoursSienceRepair() {
        return hoursSienceRepair;
    }

    public void setHoursSienceRepair(int hoursSienceRepair) {
        this.hoursSienceRepair = hoursSienceRepair;
    }

    public FlightStatus getStatus() {
        return status;
    }

    public void setStatus(FlightStatus status) {
        this.status = status;
    }

    private boolean isNotReady() {
        return this.getStatus() == FlightStatus.NOTREADYTOTAKEOFF;
    }

    private boolean isNeedRepair() {
        return this.getHoursSienceRepair() >= this.maxHoursToRepair;
    }

    public void flyTo(Coordinates destination) {
        System.out.println("Flying to:" + destination.toString());

        if (this.isNotReady()) {
            System.out.println("Aerial vehicle isn't ready to fly");
        } else {
            this.setStatus(FlightStatus.INAIR);
        }
    }

    public void land(Coordinates destination) {
        System.out.println("Landing on:" + destination.toString());
        check();
    }

    private void check() {
        if (this.isNeedRepair()) {
            setStatus(FlightStatus.NOTREADYTOTAKEOFF);
            this.repair();
        } else {
            this.setStatus(FlightStatus.READYTOTAKEOFF);
        }
    }

    private void repair() {
        this.setHoursSienceRepair(0);
        this.setStatus(FlightStatus.READYTOTAKEOFF);
    }

    public Coordinates getBase() {
        return base;
    }

    public void setBase(Coordinates base) {
        this.base = base;
    }
}
