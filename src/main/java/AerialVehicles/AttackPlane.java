package AerialVehicles;

import enums.MisselType;

public class AttackPlane implements PlaneDecorator{
    private PlaneDecorator anotherAttribute;
    private int misselsQuantity;
    private MisselType misselType;

    public AttackPlane(int misselsQuantity, MisselType misselType, PlaneDecorator anotherAttribute) {
        this.setMisselsQuantity(misselsQuantity);
        this.setMisselType(misselType);
        this.setAnotherAttribute(anotherAttribute);
    }

    public PlaneDecorator getAnotherAttribute() {
        return anotherAttribute;
    }

    public void setAnotherAttribute(PlaneDecorator anotherAttribute) {
        this.anotherAttribute = anotherAttribute;
    }

    public int getMisselsQuantity() {
        return misselsQuantity;
    }

    public void setMisselsQuantity(int misselsQuantity) {
        this.misselsQuantity = misselsQuantity;
    }

    public MisselType getMisselType() {
        return misselType;
    }

    public void setMisselType(MisselType misselType) {
        this.misselType = misselType;
    }
}
