package AerialVehicles;

import Entities.Coordinates;
import enums.FlightStatus;

public class CombatJett extends AerialVehicle{
    public CombatJett(int hoursSienceRepair, FlightStatus status, Coordinates base) {
        super(hoursSienceRepair, 250, status, base);
    }
}
