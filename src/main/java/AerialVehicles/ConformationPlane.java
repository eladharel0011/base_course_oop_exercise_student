package AerialVehicles;

import enums.CameraType;

public class ConformationPlane implements PlaneDecorator {
    private PlaneDecorator anotherAttribute;
    private CameraType cameraType;

    public ConformationPlane(CameraType cameraType, PlaneDecorator anotherAttribute) {
        this.setCameraType(cameraType);
        this.setAnotherAttribute(anotherAttribute);
    }

    public PlaneDecorator getAnotherAttribute() {
        return anotherAttribute;
    }

    public void setAnotherAttribute(PlaneDecorator anotherAttribute) {
        this.anotherAttribute = anotherAttribute;
    }

    public CameraType getCameraType() {
        return cameraType;
    }

    public void setCameraType(CameraType cameraType) {
        this.cameraType = cameraType;
    }
}
