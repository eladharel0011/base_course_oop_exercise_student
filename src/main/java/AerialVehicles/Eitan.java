package AerialVehicles;

import Entities.Coordinates;
import enums.FlightStatus;
import enums.MisselType;
import enums.SensorType;

public class Eitan extends Haron implements IAttack, IInteligence{
    private AttackPlane attributes;


    public Eitan(int hoursSienceRepair,
                 FlightStatus status,
                 int missleQuantity,
                 Coordinates base,
                 MisselType misselType,
                 SensorType sensorType) {

        super(hoursSienceRepair, status, base);
        attributes = new AttackPlane(missleQuantity,
                misselType,
                new InteligencePlane(sensorType, null));
    }

    public int getMisselsQuantity() {
        return this.attributes.getMisselsQuantity();
    }

    public void setMisselsQuantity(int misselsQuantity) {
        this.setMisselsQuantity(misselsQuantity);
    }

    public MisselType getMisselType() {
        return this.attributes.getMisselType();
    }

    public void setMisselType(MisselType misselType) {
        this.setMisselType(misselType);
    }



    public SensorType getSensorType() {
        return this.getAnotherAttribute().getSensorType();
    }

    private InteligencePlane getAnotherAttribute() {
        return (InteligencePlane) this.attributes.getAnotherAttribute();
    }
}
