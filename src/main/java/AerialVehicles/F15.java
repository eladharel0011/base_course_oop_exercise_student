package AerialVehicles;


import Entities.Coordinates;
import Missions.AerialVehicleNotCompatibleException;
import enums.FlightStatus;
import enums.MisselType;
import enums.SensorType;

public class F15 extends CombatJett implements IAttack, IInteligence{
    private AttackPlane attributes;

    public F15(int hoursSienceRepair,
               FlightStatus status,
               Coordinates base,
               int missleQuantity,
               MisselType misselType,
               SensorType sensorType) {

        super(hoursSienceRepair, status, base);
        attributes = new AttackPlane(missleQuantity,
                                     misselType,
                                     new InteligencePlane(sensorType, null));
    }


    public int getMisselsQuantity() {
        return this.attributes.getMisselsQuantity();
    }

    public void setMisselsQuantity(int misselsQuantity) {
        this.setMisselsQuantity(misselsQuantity);
    }

    public MisselType getMisselType() {
        return this.attributes.getMisselType();
    }

    public void setMisselType(MisselType misselType) {
        this.setMisselType(misselType);
    }



    public SensorType getSensorType() {
        return this.getAnotherAttribute().getSensorType();
    }

    private InteligencePlane getAnotherAttribute()  {
        return (InteligencePlane) this.attributes.getAnotherAttribute();
    }
}
