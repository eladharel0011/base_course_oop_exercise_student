package AerialVehicles;


import Entities.Coordinates;
import enums.CameraType;
import enums.FlightStatus;
import enums.MisselType;

public class F16 extends CombatJett implements IAttack, IConformation {
    private AttackPlane attributes;

    public F16(int hoursSienceRepair,
               FlightStatus status,
               Coordinates base,
               int missleQuantity,
               MisselType misselType,
               CameraType cameraType) {
        super(hoursSienceRepair, status, base);
        this.attributes = new AttackPlane(missleQuantity,
                          misselType,
                          new ConformationPlane(cameraType,null));
    }

    public int getMisselsQuantity() {
        return this.attributes.getMisselsQuantity();
    }

    public void setMisselsQuantity(int misselsQuantity) {
        this.setMisselsQuantity(misselsQuantity);
    }

    public MisselType getMisselType() {
        return this.attributes.getMisselType();
    }

    public void setMisselType(MisselType misselType) {
        this.setMisselType(misselType);
    }

    public CameraType getCameraType() {
        return this.getAnotherAttribute().getCameraType();
    }

    private ConformationPlane getAnotherAttribute() {
        return (ConformationPlane) this.attributes.getAnotherAttribute();
    }
}
