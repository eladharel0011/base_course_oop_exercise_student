package AerialVehicles;

import Entities.Coordinates;
import enums.FlightStatus;

public abstract class Hermes extends Katmam {
    public Hermes(int hoursSienceRepair, FlightStatus status, Coordinates base) {
        super(hoursSienceRepair, 100, status, base);
    }
}
