package AerialVehicles;

import Missions.AerialVehicleNotCompatibleException;
import enums.MisselType;

public interface IAttack {
   MisselType getMisselType();
   int getMisselsQuantity();
}
