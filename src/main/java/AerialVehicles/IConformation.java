package AerialVehicles;

import Missions.AerialVehicleNotCompatibleException;
import enums.CameraType;

public interface IConformation {
    CameraType getCameraType();
}
