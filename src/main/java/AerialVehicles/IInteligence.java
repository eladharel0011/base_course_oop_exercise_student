package AerialVehicles;

import Missions.AerialVehicleNotCompatibleException;
import enums.SensorType;

public interface IInteligence {
    SensorType getSensorType();
}
