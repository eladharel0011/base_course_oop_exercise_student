package AerialVehicles;

import enums.SensorType;

public class InteligencePlane implements PlaneDecorator{
    private PlaneDecorator anotherAttribute;
    private SensorType sensorType;

    public InteligencePlane(SensorType sensorType, PlaneDecorator anotherAttribute) {
        this.setSensorType(sensorType);
        this.setAnotherAttribute(anotherAttribute);
    }

    public PlaneDecorator getAnotherAttribute() {
        return anotherAttribute;
    }

    public void setAnotherAttribute(PlaneDecorator anotherAttribute) {
        this.anotherAttribute = anotherAttribute;
    }

    public SensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(SensorType sensorType) {
        this.sensorType = sensorType;
    }
}
