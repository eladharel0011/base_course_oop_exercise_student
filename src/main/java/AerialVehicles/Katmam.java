package AerialVehicles;

import Entities.Coordinates;
import enums.FlightStatus;

public abstract class Katmam extends AerialVehicle{
    public Katmam(int hoursSienceRepair, int maxHoursToRepair, FlightStatus status, Coordinates base) {
        super(hoursSienceRepair, maxHoursToRepair, status, base);
    }

    public void hoverOverLocation(Coordinates destination) {
        System.out.println("Hovering over " + destination.getLatitude() + " " +destination.getLongitude());
    }
}
