package AerialVehicles;

public interface PlaneDecorator {
    PlaneDecorator getAnotherAttribute();
}
