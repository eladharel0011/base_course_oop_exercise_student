package AerialVehicles;


import Entities.Coordinates;
import enums.CameraType;
import enums.FlightStatus;
import enums.MisselType;
import enums.SensorType;

public class Shoval extends Haron implements IAttack, IInteligence, IConformation {
    private AttackPlane attributes;

    public Shoval(int hoursSienceRepair,
                  FlightStatus status,
                  Coordinates base,
                  int missleQuantity,
                  MisselType misselType,
                  SensorType sensorType,
                  CameraType cameraType) {

        super(hoursSienceRepair, status, base);
        attributes = new AttackPlane(missleQuantity,
                                     misselType,
                                     new InteligencePlane(sensorType,
                                                            new ConformationPlane(cameraType,
                                                                     null)));
    }

    public int getMisselsQuantity() {
        return this.attributes.getMisselsQuantity();
    }

    public void setMisselsQuantity(int misselsQuantity) {
        this.setMisselsQuantity(misselsQuantity);
    }

    public MisselType getMisselType() {
        return this.attributes.getMisselType();
    }

    public void setMisselType(MisselType misselType) {
        this.setMisselType(misselType);
    }

    public SensorType getSensorType() {
        return this.getAnotherAttribute().getSensorType();
    }

    public CameraType getCameraType() {
        return this.getLastAttributes().getCameraType();
    }

    private InteligencePlane getAnotherAttribute() {
        return (InteligencePlane) this.attributes.getAnotherAttribute();
    }

    private ConformationPlane getLastAttributes() {
        return (ConformationPlane) this.getAnotherAttribute().getAnotherAttribute();
    }
}

