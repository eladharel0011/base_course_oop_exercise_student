package AerialVehicles;


import Entities.Coordinates;
import enums.CameraType;
import enums.FlightStatus;
import enums.SensorType;

public class Zik extends Hermes implements IConformation, IInteligence {
    private ConformationPlane attributes;


    public Zik(int hoursSienceRepair,
               FlightStatus status,
               Coordinates base,
               CameraType cameraType,
               SensorType sensorType) {
        super(hoursSienceRepair, status, base);
        this.attributes = new ConformationPlane(cameraType, new InteligencePlane(sensorType,null));
    }

    public CameraType getCameraType() {
        return this.attributes.getCameraType();
    }

    public SensorType getSensorType() {
        return this.getAnotherAttribute().getSensorType();
    }

    private InteligencePlane getAnotherAttribute() {
        return (InteligencePlane) this.attributes.getAnotherAttribute();
    }
}