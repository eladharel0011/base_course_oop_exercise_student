package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.AttackPlane;
import AerialVehicles.IAttack;
import Entities.Coordinates;

public class AttackMission extends Mission {
    private String target;

    public AttackMission(Coordinates destination, String pilotName, AerialVehicle plane, String target) {
        super(destination, pilotName, plane);
        this.setTarget(target);
    }
    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException {
        try {
            IAttack plane = (IAttack) (this.getPlane());
            return this.getPilotName() + ": " + this.getPlane().getClass().getSimpleName()
                    + " Attacking " + this.getTarget() + " with: " + plane.getMisselType().name()
                    + "X" + plane.getMisselsQuantity();
        } catch (Exception e) {
            throw new AerialVehicleNotCompatibleException("This plane cannot execute this mission!");
        }
    }

    public String getTarget() {
        return target;
    }

    private void setTarget(String target) {
        this.target = target;
    }
}
