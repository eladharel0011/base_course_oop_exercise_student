package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.IConformation;
import Entities.Coordinates;

public class BdaMission extends Mission {
    private String objective;

    public BdaMission(Coordinates destination, String pilotName, AerialVehicle plane, String objective) {
        super(destination, pilotName, plane);
        setObjective(objective);
    }

    @Override
    public String executeMission() throws AerialVehicleNotCompatibleException{
        try {
            IConformation plane = (IConformation) this.getPlane();
            return this.getPilotName() + ": " + this.getPlane().getClass().getSimpleName() + "taking pictures of "
                    + this.getObjective() + " with :" + plane.getCameraType().name();
        } catch (Exception e) {
            throw new AerialVehicleNotCompatibleException("This plane cannot execute this mission!");
        }

    }

    public String getObjective() {
        return objective;
    }

    private void setObjective(String objective) {
        this.objective = objective;
    }
}
