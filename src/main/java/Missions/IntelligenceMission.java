package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.IInteligence;
import Entities.Coordinates;

public class IntelligenceMission extends Mission {
    private String region;

    public IntelligenceMission(Coordinates destination, String pilotName, AerialVehicle plane, String region) {
        super(destination, pilotName, plane);
        this.setRegion(region);
    }

    @Override
    public String executeMission()  throws AerialVehicleNotCompatibleException{
        try {
            IInteligence plane = (IInteligence) this.getPlane();
            return this.getPilotName() + ": " + this.getPlane().getClass().getSimpleName() + " collecting data in "
                    + this.getRegion() + " with: sensor type: " + plane.getSensorType().name();
        } catch (Exception e) {
            throw new AerialVehicleNotCompatibleException("This plane cannot execute this mission!");
        }
    }

    public String getRegion() {
        return region;
    }

    private void setRegion(String region) {
        this.region = region;
    }
}
