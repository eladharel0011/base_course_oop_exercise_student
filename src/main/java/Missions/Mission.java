package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    private Coordinates destination;
    private String pilotName;
    private AerialVehicle plane;

    public Mission(Coordinates destination, String pilotName, AerialVehicle plane) {
        this.setDestination(destination);
        this.setPilotName(pilotName);
        this.setPlane(plane);
    }

    public abstract String executeMission() throws AerialVehicleNotCompatibleException;

    public void begin() {
        System.out.println("Beginning Mission!");
        this.getPlane().flyTo(this.getDestination());
    }

    public void cancel() {
        System.out.println("Abort Mission!");
        this.getPlane().land(this.getPlane().getBase());
    }

    public void finish() throws AerialVehicleNotCompatibleException{
        System.out.println(this.executeMission());
        this.getPlane().land(this.getPlane().getBase());
        System.out.println("Finish Mission!");
    }

    public Coordinates getDestination() {
        return destination;
    }

    public void setDestination(Coordinates destination) {
        this.destination = destination;
    }

    public String getPilotName() {
        return pilotName;
    }

    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }

    public AerialVehicle getPlane() {
        return plane;
    }

    public void setPlane(AerialVehicle plane) {
        this.plane = plane;
    }
}
