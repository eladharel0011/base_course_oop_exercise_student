package enums;

public enum CameraType {
    Regular,
    Thermal,
    NightVision
}
