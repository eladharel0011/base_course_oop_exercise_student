package enums;

public enum FlightStatus {
    INAIR,
    READYTOTAKEOFF,
    NOTREADYTOTAKEOFF
}
