import AerialVehicles.*;
import Entities.Coordinates;
import Missions.AerialVehicleNotCompatibleException;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;
import enums.CameraType;
import enums.FlightStatus;
import enums.MisselType;
import enums.SensorType;

import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        try {
            F15 f15 = new F15(249,
                    FlightStatus.READYTOTAKEOFF,
                    new Coordinates(0.,0.),
                    2,
                    MisselType.Amram,
                    SensorType.Elint);
            AttackMission mission = new AttackMission(new Coordinates(3.3, 3.3),
                    "Elad Harel",
                    f15,
                    "Gaza");
            mission.begin();
            mission.finish();

            Eitan eitan = new Eitan(99,
                    FlightStatus.READYTOTAKEOFF,
                    4,
                    new Coordinates(1.1,1.1),
                    MisselType.Spice250,
                    SensorType.InfraRed);
            IntelligenceMission mission2 = new IntelligenceMission(new Coordinates(2.2,2.2),
                    "elad",
                    eitan,
                    "my reigion");
            mission2.begin();
            mission2.finish();

            F16 sofa = new F16(251,
                    FlightStatus.READYTOTAKEOFF,
                    new Coordinates(4.4, 4.4),
                    2,
                    MisselType.Python,
                    CameraType.Thermal);
            BdaMission bdaMission = new BdaMission(new Coordinates(5.5,5.5), "Jonny Kamoni", sofa, "house");

            bdaMission.begin();
            bdaMission.finish();

            bdaMission.begin();
            bdaMission.cancel();
        } catch(AerialVehicleNotCompatibleException e) {
            System.out.println(e.getMessage());
        }
    }
}
